﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Helpers;
using System.Web.Mvc;
using WebApplication3.Models;

namespace WebApplication3.Controllers
{
    [Authorize(Roles = "Admin")]
    public class AdminController : Controller
    {
        // GET: Admin
        WebProjeDBUsers db = new WebProjeDBUsers();
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult SliderEkle()
        {
            return View();
        }
        [HttpPost]
        public ActionResult SliderEkle(Slider slide, HttpPostedFileBase foto)
        {
            //WebProjeDBESlider db = new WebProjeDBESlider()

            //WebImage img = new WebImage();
            //FileInfo fotoInfo = new FileInfo(foto.FileName);

            //string newfoto = Guid.NewGuid().ToString() + fotoInfo.Extension;
            //img.Resize(800, 400);
            //img.Save("~/Content/image/slider/" + newfoto);

            //db.Slider.Add(foto);

            //using (WebProjeDBESlider context = new WebProjeDBESlider())
            //{
            //    Slider _slide = new Slider();
            //    if (file != null && file.ContentLength > 0)
            //    {
            //        MemoryStream memoryStream = file.InputStream as MemoryStream;
            //        if (memoryStream == null)
            //        {
            //            memoryStream = new MemoryStream();
            //            file.InputStream.CopyTo(memoryStream);
            //        }
            //        _slide.SliderFoto = memoryStream.ToArray();
            //    }

            //    context.Slider.Add(_slide);
            //  int i =   context.SaveChanges();
            //    return RedirectToAction("Index", "Home");
            //}

            return RedirectToAction("Index", "Home");
        }

       
        public ActionResult Musteriler()
        {
            // WebProjeDBEntities3 dblist = new WebProjeDBEntities3();

            List<Users> users = db.Users.ToList();

            return View(users);
        }

        [HttpGet]
        public ActionResult Update(int? userid)
        {
            Users user = null;
            if (userid != null)
            {
                WebProjeDBUsers db = new WebProjeDBUsers();
                user = db.Users.Where(x => x.Id == userid).FirstOrDefault();
            }

            return View(user);
        }
        [HttpPost]
        public ActionResult Update(Users usr, int? userid)
        {

            Users users = db.Users.Where(x => x.Id == userid).FirstOrDefault();

            if (users != null)
            {
                users.UserName = usr.UserName;
                users.UserSurname = usr.UserSurname;
                users.UserEMail = usr.UserEMail;
                users.UserPassword = usr.UserPassword;

                int sonuc = db.SaveChanges();

                if (sonuc > 0)
                {
                    ViewBag.Mesaj = "true";
                }
                else
                {
                    ViewBag.Mesaj = "false";
                }
            }

            return View();
        }

        [HttpGet]
        public ActionResult Delete(int? userid)
        {
            Users usr = null;

            if (userid != null)
            {

                usr = db.Users.Where(x => x.Id == userid).FirstOrDefault();
            }

            return View(usr);
        }

        [HttpPost, ActionName("Delete")]
        public ActionResult DeleteOk(int? userid)
        {
            Users user = null;

            if (userid != null)
            {
                user = db.Users.Where(x => x.Id == userid).FirstOrDefault();

                db.Users.Remove(user);
                db.SaveChanges();
            }


            return RedirectToAction("Musteriler", "Admin");
        }


    }
}