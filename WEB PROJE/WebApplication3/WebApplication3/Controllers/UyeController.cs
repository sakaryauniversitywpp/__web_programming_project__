﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using WebApplication3.Models;

namespace WebApplication3.Controllers
{
   

    public class UyeController : Controller
    {
         WebProjeDBUsers db = new WebProjeDBUsers();

        public object FormAuthentication { get; private set; }

        // GET: Uye
        [HttpGet]
        public ActionResult Register()
        {
            return View();
        }
        [HttpPost]
        public ActionResult Register(Users model)
        {
            model.Role = "User";


            db.Users.Add(model);
            int sonuc = db.SaveChanges();

            if (sonuc > 0)
            {
                ViewBag.Mesaj = "true";
            }
            else
            {
                ViewBag.Mesaj = "false";
            }


            return View();
        }

        public ActionResult Login()
        {
            return View();
        }
        [HttpPost]
        public ActionResult Login(Users user)
        {
            var _user = db.Users.FirstOrDefault(x => x.UserEMail == user.UserEMail && x.UserPassword == user.UserPassword);

            
            if (_user != null)
            {
                _user.Role = _user.Role.Trim();

                if (_user.Role == "Admin")
                {
                    FormsAuthentication.SetAuthCookie(_user.UserName, false);

                    return RedirectToAction("Index", "Admin");
                }
                else
                {
                    FormsAuthentication.SetAuthCookie(_user.UserName, false);

                    return RedirectToAction("UserHome", "Uye");
                }
              
            }
            else
            {
                ViewBag.Mesaj = "Geçersiz E-Mail veya parola!";
                return View();
            }                 
            
        }
        public ActionResult LogOut()
        {

            FormsAuthentication.SignOut();
            return RedirectToAction("Index","Home");
        }

        



        public ActionResult UserHome()
        {
            return View();
        }
    
    }
}